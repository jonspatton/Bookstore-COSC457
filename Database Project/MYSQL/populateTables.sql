use Bookstore;

# Login permissions
insert into USER(username) values ("jpatto1");
insert into USER(username) values ("root");
insert into USER(username) values ("guest");

insert into ACCESS(username, access_level) values ("guest", "0");
insert into ACCESS(username, access_level) values ("jpatto1", "50");
insert into ACCESS(username, access_level) values ("root", "100");

# Branch and Stock
INSERT INTO `BRANCH` (`Branch_ID`, `Branch_Name`, `Address`, `Phone`) VALUES ('0001', 'Towson', '8500 York Rd. Towson, MD 21252', '4101234567');

# People
INSERT INTO `EMPLOYEE` (`ESSN`, `Fname`, `Lname`, `Eid`, `Job`, `Title`) VALUES ('987654321', 'James', 'Bond', '0001', 'Store Manager', 'Manager');
INSERT INTO `EMPLOYEE` (`ESSN`, `Fname`, `Lname`, `MSSN`, `Eid`, `Job`, `Title`) VALUES ('111223333', 'John ', 'English', '987654321', '0002', 'Cashier', 'Cashier');

INSERT INTO `CUSTOMER` (`Fname`, `Lname`, `Address`, `AccID`, `Phone`) VALUES ('John', 'Doe', '123 Road Way, Baltimore, MD', '0001', '4109876543');
INSERT INTO `CUSTOMER` (`Fname`, `Lname`, `Address`, `AccID`, `Phone`) VALUES ('Mike', 'Jones', '456 Rail Rd, Ellicott City, MD', '0002', '4433219876');

# Product Owners and Distributors
INSERT INTO `ARTIST` (`Aname`, `Artist_id`) VALUES ('Hiroyuki Suwano', '001');
INSERT INTO `ARTIST` (`Aname`, `Artist_id`) VALUES ('Imagine Dragons', '002');
INSERT INTO `ARTIST` (`Aname`, `Artist_id`) VALUES ('Lady Gaga', '003');
INSERT INTO `ARTIST` (`Aname`, `Artist_id`) VALUES ('Talking Heads', '004');

INSERT INTO `AUTHOR` (`Fname`, `Lname`, `Aid`) VALUES ('J.K.', 'Rowling', '001');
INSERT INTO `AUTHOR` (`Fname`, `Lname`, `Aid`) VALUES ('Brandon', 'Sanderson', '002');
INSERT INTO `AUTHOR` (`Fname`, `Lname`, `Aid`) VALUES ('Robert', 'Jordan', '003');
INSERT INTO `AUTHOR` (`Fname`, `Lname`, `Aid`) VALUES ('J.R.R.', 'Tolkien', '004');

INSERT INTO `PUBLISHER` (`Pname`, `Paddress`, `Phone`) VALUES ('Scholastic', '557 Broadway, New York, NY', '8007246527');
INSERT INTO `PUBLISHER` (`Pname`, `Paddress`, `Phone`) VALUES ('Random House', '558 Broadway, New York, NY', '8007246528');
INSERT INTO `PUBLISHER` (`Pname`, `Paddress`, `Phone`) VALUES ('Tom Doherty Assosiates', '559 Broadway, New York, NY', '8007246529');
INSERT INTO `PUBLISHER` (`Pname`, `Paddress`, `Phone`) VALUES ('Time Magazine Co', '560 Broadway, New York, NY', '8007246530');
INSERT INTO `PUBLISHER` (`Pname`, `Paddress`, `Phone`) VALUES ('Sony Music', '561 Broadway, New York, NY', '8007246531');

INSERT INTO `MANUFACTURER` (`M_Name`, `Address`, `Phone`) VALUES ('Playmake', '777 Play Dr, New York, NY', '7777777777');

# Product info
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0001', 'Book', '9.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0002', 'Book', '9.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0003', 'Book', '9.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0004', 'Book', '8.50');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0005', 'Book', '9.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0006', 'Book', '8.50');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0007', 'Book', '18.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0008', 'Drink', '8.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0009', 'Drink', '4.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0010', 'Food', '5.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0011', 'Magezine', '2.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0012', 'Misc', '1.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0013', 'Movie', '20.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0014', 'Music', '10.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0015', 'Music', '10.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0016', 'Music', '10.00');
INSERT INTO `PRODUCT` (`Item_Code`, `Item_Type`, `Price`) VALUES ('0017', 'Music', '10.00');

INSERT INTO `BOOK` (`ISBN`, `Item_Code`, `Title`, `Aid`, `Publisher_Name`, `Pages`) VALUES ('9780590353427', '0001', 'Harry Potter and the Sorceror\'s Stone', '001', 'Scholastic', '450');
INSERT INTO `BOOK` (`ISBN`, `Item_Code`, `Title`, `Aid`, `Publisher_Name`, `Pages`) VALUES ('9780439064873', '0002', 'Harry Potter and the Chamber of Secrets ', '001', 'Scholastic', '350');
INSERT INTO `BOOK` (`ISBN`, `Item_Code`, `Title`, `Aid`, `Publisher_Name`, `Pages`) VALUES ('9780439136365', '0003', 'Harry Potter and the Prisoner of Azkaban', '001', 'Scholastic', '450');
INSERT INTO `BOOK` (`ISBN`, `Item_Code`, `Title`, `Aid`, `Publisher_Name`, `Pages`) VALUES ('9780345339706', '0004', 'Lord of the Rings: Fellowship of the Ring', '004', 'Random House', '480');
INSERT INTO `BOOK` (`ISBN`, `Item_Code`, `Title`, `Aid`, `Publisher_Name`, `Pages`) VALUES ('9780547952017', '0005', 'Lord of the Rings: Fellowship of the Ring', '004', 'Houghton Mifflin', '430');
INSERT INTO `BOOK` (`ISBN`, `Item_Code`, `Title`, `Aid`, `Publisher_Name`, `Pages`) VALUES ('9780812511819', '0006', 'Wheel of Time: The Eye of the World', '003', 'Tom Doherty Associates', '830');
INSERT INTO `BOOK` (`ISBN`, `Item_Code`, `Title`, `Aid`, `Publisher_Name`, `Pages`) VALUES ('9781429992800', '0007', 'Stormlight Archive: The Way of Kings', '002', 'Tom Doherty Associates', '1000');

INSERT INTO `DRINK` (`Item_Code`, `Name`, `Manufacturer_name`, `Description`) VALUES ('0008', 'Coffee', 'Playmake', 'Coffee');
INSERT INTO `DRINK` (`Item_Code`, `Name`, `Manufacturer_name`, `Description`) VALUES ('0009', 'Mint Hot Chocolate', 'Playmake', 'Hot Chocolate with Mint');

INSERT INTO `FOOD` (`Item_Code`, `Name`, `Manufacturer_name`, `Description`) VALUES ('0010', 'Hambugrer', 'Playmake', 'Hamburger');

INSERT INTO `FORMATTYPE` (`Item_Code`, `Format_Type`) VALUES ('0001', 'Paperback');
INSERT INTO `FORMATTYPE` (`Item_Code`, `Format_Type`) VALUES ('0002', 'Paperback');
INSERT INTO `FORMATTYPE` (`Item_Code`, `Format_Type`) VALUES ('0003', 'Paperback');
INSERT INTO `FORMATTYPE` (`Item_Code`, `Format_Type`) VALUES ('0004', 'Paperback');
INSERT INTO `FORMATTYPE` (`Item_Code`, `Format_Type`) VALUES ('0005', 'eBook');
INSERT INTO `FORMATTYPE` (`Item_Code`, `Format_Type`) VALUES ('0006', 'Paperback');
INSERT INTO `FORMATTYPE` (`Item_Code`, `Format_Type`) VALUES ('0007', 'Paperback');

INSERT INTO `MAGAZINE` (`Item_Code`, `Title`, `Publisher_name`) VALUES ('0011', 'Time', 'Time Magazine Co');

INSERT INTO `MISC` (`Item_Code`, `Name`, `Manufacturer_name`, `Description`) VALUES ('0012', 'Playing Cards', 'Playmake', 'Deck of playing cards');

INSERT INTO `MOVIE` (`EIDR`, `Item_Code`, `Title`, `Publisher_name`, `Minutes`) VALUES ('0001', '0013', 'Avengers: Infinity War', 'Disney', '149');

INSERT INTO `MUSIC` (`ISRC`, `Item_Code`, `Title`, `Artist_id`, `Publisher_name`) VALUES ('0001', '0014', 'Evolve', '0002', 'Sony Music');
INSERT INTO `MUSIC` (`ISRC`, `Item_Code`, `Title`, `Artist_id`, `Publisher_name`) VALUES ('0002', '0015', 'Into the Sky', '0001', 'Sony Music');
INSERT INTO `MUSIC` (`ISRC`, `Item_Code`, `Title`, `Artist_id`, `Publisher_name`) VALUES ('0003', '0016', 'Artpop', '0003', 'Sony Music');
INSERT INTO `MUSIC` (`ISRC`, `Item_Code`, `Title`, `Artist_id`, `Publisher_name`) VALUES ('0004', '0017', 'Once in a Lifetime', '0004', 'Sony Music');

INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0001', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0002', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0003', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0004', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0005', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0006', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0007', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0008', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0009', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0010', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0011', '10');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0012', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0013', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0014', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0015', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0016', '1');
INSERT INTO `STOCK` (`Branch_ID_Located`, `Item_Code`, `Quantity`) VALUES ('0001', '0017', '1');

#Transaction and Rewards info
INSERT INTO `TRANSACTION_RECORD` (`DateOf`, `Amount`, `Item_Id`, `Trans_Id`) VALUES ('11/24/2018', '1', '0005', '0001');
INSERT INTO `TRANSACTION_RECORD` (`DateOf`, `Amount`, `Item_Id`, `Trans_Id`) VALUES ('11/24/2018', '1', '0012', '0001');
INSERT INTO `TRANSACTION_RECORD` (`DateOf`, `Amount`, `Item_Id`, `Trans_Id`) VALUES ('11/24/2018', '2', '0015', '0001');
INSERT INTO `TRANSACTION_RECORD` (`DateOf`, `Amount`, `Item_Id`, `Trans_Id`) VALUES ('12/01/2018', '1', '0009', '0002');
INSERT INTO `TRANSACTION_RECORD` (`DateOf`, `Amount`, `Item_Id`, `Trans_Id`) VALUES ('12/01/2018', '1', '0010', '0002');


INSERT INTO `REWARDS_ACCOUNT` (`A_Id`, `CurrPoints`) VALUES ('0001', '500');
INSERT INTO `REWARDS_ACCOUNT` (`A_Id`, `CurrPoints`) VALUES ('0002', '000');

INSERT INTO `REWARDS_TRANSACTION` (`A_Id`, `Date_Of`, `Points_Transaction`, `Trans_ID`) VALUES ('0001', '11/24/2018', '400', '0001');
INSERT INTO `REWARDS_TRANSACTION` (`A_Id`, `Date_Of`, `Points_Transaction`, `Trans_ID`) VALUES ('0001', '12/01/2018', '100', '0002');

