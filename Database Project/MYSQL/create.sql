create schema if not exists Bookstore;

use Bookstore;

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE IF NOT EXISTS PRODUCT
(Item_Code varchar(12) NOT NULL,
Item_Type varchar(15),
Price double not null,
PRIMARY KEY (Item_Code));

CREATE TABLE IF NOT EXISTS MAGAZINE
(Item_Code char(12) not null,
Title varchar(50),
Publisher_name varchar(30) not null,
PRIMARY KEY (Item_Code),
FOREIGN KEY(Item_Code) references Product(Item_Code),
FOREIGN KEY(Publisher_Name) references Publisher(Pname));

CREATE TABLE IF NOT EXISTS MOVIE
(EIDR char(42) not null,
Item_Code char(12) not null,
Title varchar(50),
Publisher_name varchar(30) not null,
Minutes int,
PRIMARY KEY (EIDR),
FOREIGN KEY(Item_Code) references Product(Item_Code),
FOREIGN KEY(Publisher_Name) references Publisher(PName));

CREATE TABLE IF NOT EXISTS STOCK
(Branch_ID_Located VARCHAR(15) not null,
Item_Code varchar(12) not null,
Quantity int,
PRIMARY KEY (Branch_ID_Located, Item_Code),
FOREIGN KEY (Branch_ID_Located) references Branch(Branch_ID),
FOREIGN KEY (Item_Code) references Product(Item_Code));

CREATE TABLE IF NOT EXISTS PUBLISHER
(Pname varchar(30) NOT NULL,
Paddress varchar(50) NOT NULL,
Phone Char(10) NOT NULL,
PRIMARY KEY (Pname));

CREATE TABLE IF NOT EXISTS AUTHOR
(Fname VARCHAR(15) NOT NULL,
Lname VARCHAR(15)    NOT NULL,
Aid    varchar(15)    NOT NULL,
PRIMARY KEY (Aid));

CREATE TABLE IF NOT EXISTS ARTIST
(Aname varchar(25) NOT NULL,
Artist_id    varchar(15)    NOT NULL,
PRIMARY KEY (Artist_id));

CREATE TABLE IF NOT EXISTS EMPLOYEE
(ESSN VarChar(9) NOT NULL,
Fname VarChar(15) NOT NULL,
Lname VarChar(15) NOT NULL,
MSSN VarChar(9),
Eid varchar(15) NOT NULL,
Job    Varchar(15),
Title varchar(15),
PRIMARY KEY (ESSN, Eid));

CREATE TABLE IF NOT EXISTS TRANSACTION_RECORD
(DateOf VARCHAR(15),
Amount INT NOT NULL,
Item_Id VARCHAR(15) NOT NULL,
Trans_Id INT NOT NULL,
PRIMARY KEY (Trans_Id, Dateof, Item_Id));

CREATE TABLE IF NOT EXISTS BRANCH
(Branch_ID VARCHAR(15) NOT NULL,
Branch_Name CHAR(15),
Address VARCHAR(50),
Phone char(10),
PRIMARY KEY (Branch_ID));

CREATE TABLE IF NOT EXISTS MANUFACTURER
(M_Name CHAR(15) NOT NULL,
Address VARCHAR(50),
Phone char(10),
PRIMARY KEY (M_NAME));

CREATE TABLE IF NOT EXISTS REWARDS_ACCOUNT
(A_Id VARCHAR(15) NOT NULL,
CurrPoints INT,
PRIMARY KEY (A_Id),
Foreign key (A_id) references CUSTOMER(AccID));

CREATE TABLE IF NOT EXISTS CUSTOMER 
(Fname VARCHAR(15),
Lname VARCHAR(15),
Address VARCHAR(50),
AccID    VARCHAR(15) NOT NULL,
Phone    Char(10),
PRIMARY KEY (AccID));

CREATE TABLE IF NOT EXISTS REWARDS_TRANSACTION 
(A_Id VARCHAR(15) NOT NULL,
Date_Of VARCHAR(15),
Points_Transaction INT,
Trans_ID INT NOT NULL,
PRIMARY KEY (A_Id,Trans_Id,Date_Of),
Foreign Key (Trans_ID) References Transaction_Record(Trans_ID),
Foreign Key (Date_Of) References Transaction_Record(Date_Of),
Foreign Key (A_Id) References Customer(AccID));

CREATE TABLE IF NOT EXISTS BOOK
(ISBN char(13) not null,
Item_Code varchar(12) not null,
Title varchar(50),
Aid varchar(15) not null,
Publisher_Name varchar(30) not null,
Pages int,
PRIMARY KEY (ISBN),
FOREIGN KEY(Item_Code) references Product(Item_Code),
FOREIGN KEY(Aid) references Author(Aid),
FOREIGN KEY(Publisher_Name) references Publisher(PName));

CREATE TABLE IF NOT EXISTS MUSIC
(ISRC char(12) not null,
Item_Code char(12) not null,
Title varchar(50),
Artist_id varchar(15),
Publisher_name varchar(30) not null,
PRIMARY KEY (ISRC),
FOREIGN KEY(Item_Code) references Product(Item_Code),
FOREIGN KEY(Artist_id) references Artist(Artist_id));

CREATE TABLE IF NOT EXISTS FORMATTYPE
(Item_Code varchar(12) not null,
Format_Type varchar(15) not null,
Primary key(Item_Code, Format_Type),
Foreign key(Item_Code) references Product(Item_Code));

CREATE TABLE IF NOT EXISTS FOOD
(Item_Code varchar(12) not null,
Name varchar(20) not null,
Manufacturer_name varchar(20),
Description varchar(100),
Foreign key(Item_Code) references Product(Item_Code),
Foreign key (Manufacturer_name) references MANUFACTURER(M_name));

CREATE TABLE IF NOT EXISTS DRINK
(Item_Code varchar(12) not null,
Name varchar(20) not null,
Manufacturer_name varchar(20),
Description varchar(100),
Foreign key(Item_Code) references Product(Item_Code),
Foreign key (Manufacturer_name) references MANUFACTURER(M_name));

CREATE TABLE IF NOT EXISTS MISC
(Item_Code varchar(12) not null,
Name varchar(20) not null,
Manufacturer_name varchar(20),
Description varchar(100),
Foreign key(Item_Code) references Product(Item_Code),
Foreign key (Manufacturer_name) references MANUFACTURER(M_name));

CREATE TABLE IF NOT EXISTS USER
(username varchar(25) not null,
Primary key(username));

CREATE TABLE IF NOT EXISTS ACCESS
(username varchar(25) not null,
access_level int not null,
Primary key(username),
Foreign key(username) references user(username));