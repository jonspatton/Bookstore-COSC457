use bookstore;

insert into user(username) values ("jpatto1");
insert into user(username) values ("root");
insert into user(username) values ("guest");

insert into access(username, access_level) values ("guest", "0");
insert into access(username, access_level) values ("jpatto1", "50");
insert into access(username, access_level) values ("root", "100");