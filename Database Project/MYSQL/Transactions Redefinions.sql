use bookstore;

drop table Rewards_Transaction;
drop table Transaction_Record;

CREATE TABLE IF NOT EXISTS REWARDS_TRANSACTION 
(A_Id VARCHAR(15) NOT NULL,
Date_Of VARCHAR(15),
Points_Transaction INT,
Trans_ID INT NOT NULL,
PRIMARY KEY (A_Id),
Foreign Key (Trans_ID) References Transaction_Record(Trans_ID));

CREATE TABLE IF NOT EXISTS TRANSACTION_RECORD
(DateOf VARCHAR(15),
Amount INT NOT NULL,
Item_Id VARCHAR(15) NOT NULL,
Trans_Id INT NOT NULL,
Quantity INT not null,
PRIMARY KEY (Trans_Id, Item_ID));