use bookstore;

CREATE TABLE IF NOT EXISTS USER
(username varchar(25) not null,
Primary key(username));

CREATE TABLE IF NOT EXISTS ACCESS
(username varchar(25) not null,
access_level int not null,
Primary key(username),
Foreign key(username) references user(username));