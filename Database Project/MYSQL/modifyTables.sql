use bookstore;

alter table MAGAZINE modify Title varchar(50);
alter table book modify Title varchar(50);
alter table MOVIE modify Title varchar(50);
alter table Music modify Title varchar(50);

alter table PUBLISHER modify Phone Char(10) NOT NULL, modify Paddress varchar(25) NOT NULL;

alter table author modify Fname varchar(15) NOT NULL,
modify Lname varchar(15) NOT NULL;

alter table artist modify Aname varchar(25) NOT NULL;

drop table if exists employee;

CREATE TABLE IF NOT EXISTS EMPLOYEE
(ESSN VarChar(9) NOT NULL,
Fname VarChar(15) NOT NULL,
Lname VarChar(15) NOT NULL,
MSSN VarChar(9),
Eid varchar(15) NOT NULL,
Job    Varchar(15),
Title varchar(15),
PRIMARY KEY (ESSN, Eid));

drop table if exists REWARDS_ACCOUNT;

CREATE TABLE IF NOT EXISTS REWARDS_ACCOUNT
(A_Id VARCHAR(15) NOT NULL,
CurrPoints INT,
PRIMARY KEY (A_Id));

alter table BRANCH modify Phone char(10), modify Address VARCHAR(25);

alter table Customer modify Address VARCHAR(25), modify Phone CHAR(10),
modify Fname varchar(15) NOT NULL,
modify Lname varchar(15) NOT NULL;
modify Phone Char(10);

alter table food modify description varchar(100);
alter table drink modify description varchar(100);
alter table misc modify description varchar(100);

alter table formattype drop Primary key, add Primary key(Item_Code, Format_Type);

alter table employee add Fname varchar(15) NOT NULL,
add Lname varchar(15) NOT NULL;
