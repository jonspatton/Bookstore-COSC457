/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project;

/**
 * @author Kyle Clabough
 * @author Jon Patton
 * @class COSC
 *
 */
import database.project.Menus.MainMenu;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Gathers login information and saves a connection.
 * @author admin
 */
public class DatabaseManager
{

    // JDBC driver name and database URL
    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public String JDBC_DB_URL;
    public String DB_ENDPOINT;
    public String SCHEMA;

    //  Database credentials
    public String username;
    public String password;

    private Connection connect;
    private Statement statement;
    
    private static DatabaseManager dbinstance;
    private UserState ustate;

    /**
     * Constructor
     */
    private DatabaseManager()
    {
        connect = null;
        statement = null;

        startup();
    }
    
    
    /**
     * Runs this object.
     */
    private void startup()
    {
        new LoginForm().run();
    }
    
    public static DatabaseManager getInstance()
    {
        if (dbinstance == null)
        {
            dbinstance = new DatabaseManager();
        }
        return dbinstance;
    }
    
    /**
     * Saves the state of the user who has logged in.
     */
    private void setState()
    {
        if (connect != null) {
            setDatabase();
            ustate = new UserState();
        }
    }
    
    /**
     * Saves the username, password, and actual database point provided.
     * @param username
     * @param password
     * @param endpoint 
     */
    private void setCredentials(String username, String password, String endpoint)
    {
        this.username = username;
        this.password = password;
        DB_ENDPOINT = endpoint;
        JDBC_DB_URL = "jdbc:mysql://" + DB_ENDPOINT + "?useSSL=false";
    }
    
    /**
     * Saves a new schema (for the database) to use with the db objects.
     * @param schema 
     */
    private void setSchema(String schema)
    {
        SCHEMA = schema;
    }

    /**
     * @return the connection object to the database. Needed for querying.
     */
    public Connection getConnection()
    {
        return connect;
    }

    /**
     * Runs the driver and sets the connection to the database based on the
     * provided credentials.
     */
    private void setConnection()
    {
        
        try
        {
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER).newInstance();
            //STEP 3: Open a connection
            //System.out.println("Connecting to a selected database...");
            connect = DriverManager.getConnection(JDBC_DB_URL, username, password);
            //print on console
            new PopupMsg("Connected database successfully...").run();
            statement = connect.createStatement();

        }
        catch (SQLException se)
        {
            //se.printStackTrace();
            new PopupMsg("SQL error with credentials.").run();
            connect = null;
            dbinstance = null;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Uses the driver to set what schema the database will use during connection.
     */
    private void setDatabase()
    {   
        runStatement("use " + SCHEMA);
    }
    private void setDatabase(String schema)
    {   
        this.SCHEMA = schema;
        runStatement("use " + SCHEMA);
    }

    /**
     * Connects to the database; requires a useable schema (that is, the database
     * actually exists).
     * @param statement 
     */
    private void runStatement(String statement)
    {
        try
        {
            connect.createStatement().execute(statement);
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            new PopupMsg("Schema name failed.");
            dbinstance = null;
            connect = null;
        }
    }

    /**
     * **CAUTION!!!** Explicitly close the database connection.
     * This database manager uses a singleton. This should be
     * called ONLY when the user is logging off and the connection
     * will not be needed again until they log in again!
     */
    public void closeConnection()
    {
        //used to close resources
        try
        {
            if (connect != null)
            {
                connect.close(); //close connection
            }
        }
        catch (SQLException se)//Handle errors
        {
            se.printStackTrace();
        }
    }
    
    /* ************************************************************************
    Getters
    ************************************************************************ */
    /**
     * @return the schema we're using. (For queries)
     */
    public String getSCHEMA() {
        return SCHEMA;
    }
    /**
     * @return the username of the person currently logged in.
     */
    public String getUserName(){
        return username;
    }
    
    /**
     * @return the admin level credentials of the user.
     */
    public int getAdminLevel(){
        return ustate.adminLevel;
    }
    
    /**
     * Inner class to maintain the user credentials.
     */
    private class UserState {
        private int adminLevel;
    
        /**
         * Constructor.
         */
        UserState()
        {
            setAdminLevel();
        }
        
        /**
         * Looks up the user's access level in the database and verifies it.
         */
        private void setAdminLevel()
        {
            try{
                String q = "SELECT access_level FROM " + SCHEMA + ".`access` "
                    + "where username = \"" + username + "\" LIMIT 1";
                Statement stmt = connect.prepareStatement(q);
                ResultSet rs = stmt.executeQuery(q);
                rs.next();
                adminLevel = rs.getInt("access_level");
                
            }
            catch(Exception e)
            {
                //System.err.println("Error obtaining admin level. E-mail your administrator"
                        //+ "to verify your access level.");
                new PopupMsg("Error obtaining admin level. E-mail your administrator"
                        + "to verify your access level.").run();
                adminLevel = 0;
            }
        }
    }

    /**
     * A little form for making logging in more pleasant.
     */
    private class LoginForm extends javax.swing.JFrame
    {
        /**
         * Constructor.
         */
        LoginForm()
        {
            initComponents();
        }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jInternalFrame1 = new javax.swing.JInternalFrame();
        jLabel2 = new javax.swing.JLabel();
        submitButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        loginField = new javax.swing.JTextField();
        jPasswordField1 = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        endpointField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        schemaField = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jInternalFrame1.setTitle("Login");
        jInternalFrame1.setVisible(true);

        jLabel2.setText("Password");

        submitButton.setText("Login");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Login");

        jPasswordField1.setText("");

        jLabel3.setText("DB ENDPOINT");

        jLabel4.setText("DB Name (schema)");

        javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame1Layout.createSequentialGroup()
                .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jInternalFrame1Layout.createSequentialGroup()
                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jInternalFrame1Layout.createSequentialGroup()
                                .addGap(122, 122, 122)
                                .addComponent(submitButton))
                            .addGroup(jInternalFrame1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel3)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jInternalFrame1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInternalFrame1Layout.createSequentialGroup()
                                .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))
                                .addGap(90, 90, 90)
                                .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jPasswordField1)
                                    .addComponent(loginField)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInternalFrame1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(endpointField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jInternalFrame1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                                .addComponent(schemaField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(loginField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(endpointField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(schemaField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(79, 79, 79)
                .addComponent(submitButton)
                .addGap(107, 107, 107))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jInternalFrame1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jInternalFrame1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>                        

        private void submitButtonActionPerformed(java.awt.event.ActionEvent evt)                                             
        {                                                 
            save();
            this.dispose();
        }                                            

        /**
         * Save the user's credentials.
         */
        private void save()
        {
            username = loginField.getText().trim();
            DB_ENDPOINT = endpointField.getText().trim();
            SCHEMA = schemaField.getText().trim();
            char[] passwordArray = jPasswordField1.getPassword();
            //password starts at 1
            /*
            for (int i = 1; i < passwordArray.length; i++)
            {
                password = password + passwordArray[i];
            }*/
            password = jPasswordField1.getText();

            DatabaseManager db = DatabaseManager.getInstance();
            db.setCredentials(username, password, DB_ENDPOINT);
            db.setConnection();
            db.setState();

            
            if (db.connect == null)
            {
                new LoginForm().run();
                this.dispose();
            }
            else
            {
                new MainMenu().run();
                this.dispose();
            }
        }
    
        /**
         * Display and run the form.
         */
        void run()
        {
            /* Set the Nimbus look and feel */
            //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
            //</editor-fold>
        

            /* Create and display the form */
            java.awt.EventQueue.invokeLater(new Runnable()
            {
                public void run()
                {
                    new LoginForm().setVisible(true);
                }
            });
        }

        // Variables declaration - do not modify                     
        private javax.swing.JTextField endpointField;
        private javax.swing.JInternalFrame jInternalFrame1;
        private javax.swing.JLabel jLabel1;
        private javax.swing.JLabel jLabel2;
        private javax.swing.JLabel jLabel3;
        private javax.swing.JLabel jLabel4;
        private javax.swing.JPasswordField jPasswordField1;
        private javax.swing.JTextField loginField;
        private javax.swing.JTextField schemaField;
        private javax.swing.JButton submitButton;
        // End of variables declaration                   
    }
}
