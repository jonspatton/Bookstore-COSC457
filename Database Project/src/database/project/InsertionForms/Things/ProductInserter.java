/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project.InsertionForms.Things;

import database.project.DatabaseManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jdk.nashorn.internal.objects.NativeFunction.call;

/**
 * Helper class so that we can insert things automatically from other classes.
 * Unfortunately this doesn't actually help since we're using transactions for
 * the inserts in other tables, and you can't run this in the middle of a transaction.
 * @author Jon S. Patton
 */
public class ProductInserter {
                                                
    /**
     * Helper method across all salable items to insert into the product table.
     * @param item_code
     * @param item_type
     * @param price
     * @param con
     * @param schema
     * @throws SQLException 
     */
    public static void insert(String item_code, String item_type, String price, 
            Connection con, String schema) throws SQLException
    {            
            String call = "INSERT INTO `" + schema + "`.`Product` (`Item_Code`, `Item_Type`, `Price`)"
                + "VALUES (?, ?, ?)";
            
            PreparedStatement prepState = con.prepareStatement(call);
            prepState.setString(1, item_code);
            prepState.setString(2, item_type);
            prepState.setString(3, price);
            prepState.execute();
    }
    
}
