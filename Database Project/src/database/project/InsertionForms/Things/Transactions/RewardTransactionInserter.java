/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project.InsertionForms.Things.Transactions;

import database.project.InsertionForms.Things.*;
import database.project.DatabaseManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jdk.nashorn.internal.objects.NativeFunction.call;

/**
 * Creates a reward transaction record.
 * @author Jon S. Patton
 */
public class RewardTransactionInserter {
                                                
    /**
     * Creates a rewards transaction record.
     * @param customer_account_id
     * @param date
     * @param points
     * @param trans_ID
     * @param con
     * @param schema
     * @throws SQLException 
     */
    public static void insert(String customer_account_id, String date, String points, 
            String trans_ID, Connection con, String schema) throws SQLException
    {            
            String call = "INSERT INTO `" + schema + "`.`REWARDS_TRANSACTION` "
                    + "(`A_Id`, `Date_Of`, `Points_Transaction`, `Trans_ID`)"
                + "VALUES (?, ?, ?, ?)";
            
            PreparedStatement prepState = con.prepareStatement(call);
            prepState.setString(1, customer_account_id);
            prepState.setString(2, date);
            prepState.setString(3, points);
            prepState.setString(4, points);
            prepState.execute();
    }
    
    /**
     * Updates an existing account's points.
     */
    private void updateRewardsAccountPoints(){
    /*
    To do: This needs a method to UPDATE the points on the rewards account based on
    the points spent.
    */    
    }
    
}
