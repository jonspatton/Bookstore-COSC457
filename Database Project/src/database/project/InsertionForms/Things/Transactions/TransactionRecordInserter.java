/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project.InsertionForms.Things.Transactions;

import database.project.InsertionForms.Things.*;
import database.project.DatabaseManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jdk.nashorn.internal.objects.NativeFunction.call;

/**
 * Creates a transaction record.
 * @author Jon S. Patton
 */
public class TransactionRecordInserter {
                          
    /**
     * Run the insert statement for a transaction record. (Helper method for the form.)
     * @param dateOf
     * @param amount
     * @param item_id
     * @param trans_id
     * @param quantity
     * @param con
     * @param schema
     * @throws SQLException 
     */
    public static void insert(String dateOf, String amount, String item_id, 
            String trans_id, String quantity,
            Connection con, String schema) throws SQLException
    {            
            String call = "INSERT INTO `" + schema + "`.`TRANSACTION_RECORD` "
                    + "(`DateOf`, `Amount`, `Item_Id`, `Trans_Id`, `Quantity`)"
                + "VALUES (?, ?, ?, ?, ?)";
            
            PreparedStatement prepState = con.prepareStatement(call);
            prepState.setString(1, dateOf);
            prepState.setString(2, amount);
            prepState.setString(3, item_id);
            prepState.setString(4, trans_id);
            prepState.setString(5, quantity);
            prepState.execute();
    }
    
}
