/**
 * Unused.
 * From https://stackoverflow.com/questions/10819469/hide-input-on-command-line
 * http://www.cse.chalmers.se/edu/course/TDA602/Eraserlab/pwdmasking.html
 */

package database.project;
import java.io.*;

/**
 * Class to hide console input from the screen.
 * @author admin
 */
class EraserThread implements Runnable {
   private boolean stop;

   /**
    *@param The prompt displayed to the user
    */
   public EraserThread(String prompt) {
       System.out.print(prompt);
   }

   /**
    * Begin masking...display asterisks (*)
    */
   public void run () {
      stop = true;
      while (stop) {
         System.out.print("\010*");
     try {
        Thread.currentThread().sleep(1);
         } catch(InterruptedException ie) {
            ie.printStackTrace();
         }
      }
   }

   /**
    * Instruct the thread to stop masking
    */
   public void stopMasking() {
      this.stop = false;
   }
}