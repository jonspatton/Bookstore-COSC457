/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project;

import java.util.Arrays;

/**
 *
 * @author Jon Patton
 */
public class CONSTANTS {
    public String DBNAME = "";
    public static final int DBGLOBALACCESS = 207;
    public static final int DBADMINACCESS = 100;
    public static final int DBEMPLOYEEACCESS = 26;
    
    /**
     * Tables requiring admin access.
     */
    private static String[] ADMINTABLES = {"ACCESS", "EMPLOYEES", "USER"};
    
    /**
     * Tables requiring employee level access.
     */
    private static String[] EMPLACCESSTABLES = {"CUSTOMER", "REWARDS_ACCOUNT", 
        "REWARDS_TRANSACTION", "TRANSACTION_RECORD"};
    
    /**
     * This checks the user's credentials against some hard-coded values.
     * Ideally this should actually be handled at the database level but
     * we didn't really have the login set up to handle that.
     * @param s
     * @param accessLevel
     * @return 
     */
    public static boolean accessLevelRequired(String s, int accessLevel)
    {
        if (Arrays.asList(ADMINTABLES).contains(s))
        {
            return accessLevel >= DBADMINACCESS;
        }
        if (Arrays.asList(EMPLACCESSTABLES).contains(s))
        {
            return accessLevel >= DBEMPLOYEEACCESS;
        }
        return true;
    }
    
}
