/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project;

import database.project.Menus.MainMenu;

/**
 * Creates a database manager, which will in turn log in and initialize the system.
 */
public class DatabaseProject
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        DatabaseManager db = DatabaseManager.getInstance();
    }

}
