/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project.QueryForms;

import database.project.QueryForms.QueryTools;
import database.project.DatabaseManager;
import database.project.PopupMsg;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * @author Kyle Clabough
 * @author Jon S. Patton
 *
 */
public class UpdateTable extends JFrame
{

    private JFrame inventoryFrame;
    private JTable inventoryTable;
    private DatabaseManager db;
    private int column_num;
    private String table;
    private int row_num;
    private String schema;
    private String SQuery;
    
    private String[] columnNames;
    private String[] newValues;
    private String[] currentValues;
    private Object[][] cellData;

    /**
     * Default constructor: Gets everything
     */
    public UpdateTable(String SQuery)
    {
        db = DatabaseManager.getInstance();
        schema = db.getSCHEMA();
        this.SQuery = SQuery;
    }
    
    /**
     * Blank constructor.
     */
    public UpdateTable(){
        db = DatabaseManager.getInstance();
        schema = db.getSCHEMA();
    }
    
    /**
     * Sets the query that will be used for the update.
     * @param query 
     */
    public void setSQuery(String query)
    {
        this.SQuery = query;
    }
    
    /**
     * @return The query that has been saved by this.
     */
    public String getSQuery(){
        return this.SQuery;
    }
    
    /**
     * Execute an update q.
     * @param q the query.
     */
    public void update(String q)
    {
        setSQuery(q);
        update();
    }
    
    /**
     * Execute the current query.
     */
    public void update()
    {
        try
        {
            Connection con = db.getConnection();
            con.setAutoCommit(false); //transaction block start
            
            PreparedStatement prepState = con.prepareStatement("SET FOREIGN_KEY_CHECKS=0");
            prepState.execute();
            
            prepState = db.getConnection().prepareStatement(SQuery);
            prepState.executeUpdate();
            
            con.commit(); //transaction block end
            con.setAutoCommit(true); //transaction block start
        }
        catch (SQLException ex)
        {
            new PopupMsg("Could not update value. Please check the key and\n"
                    + "your user's manual to ensure the correct format of the\n"
                    + "value you are updating and to ensure that you are not\n"
                    + "attempting to update a key value.").run();
            //Logger.getLogger(NewMusic.class.getName()).log(Level.SEVERE, null, ex);
            //System.out.println("Could not update value. Please check the key and\n"
                    //+ "your user's manual to ensure the correct format of the\n"
                    //+ "value you are updating and to ensure that you are not\n"
                    //+ "attempting to update a key value.");
        }
    }
}
