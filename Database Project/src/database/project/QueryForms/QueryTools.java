/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project.QueryForms;

import database.project.CONSTANTS;
import database.project.DatabaseManager;
import database.project.PopupMsg;
import java.sql.*;
import java.util.ArrayList;

/**
 * Class of helper methods for queries.
 * @author Jon Patton
 */
public class QueryTools {
           
    /**
     * Builds a string array with the names of all columns in a table 
     * based on the table name.
     * @param db
     * @param schema
     * @param table
     * @return 
     */
    public static String[] getColumnNames(DatabaseManager db, String schema, String table)
    {
        String SQuery = "describe " + schema + ".`" + table + "`";
        return getter("Field", SQuery, db);
    }
    
    
    /**
     * Builds a string array with the column names in a RESULT SET.
     * https://stackoverflow.com/questions/696782/retrieve-column-names-from-java-sql-resultset
     */
    public static String[] getColumnNames(ResultSet rs, ResultSetMetaData rsmd)
    {
        ArrayList<String> names = new ArrayList<String>();
        try{
            int columnCount = rsmd.getColumnCount();

            // The column count starts from 1
            for (int i = 1; i <= columnCount; i++ ) {
                String name = rsmd.getColumnName(i).toUpperCase();
                names.add(name);
            }
        }
        catch (Exception e)
        {
            //System.err.println("Error getting columns from query.");
            new PopupMsg("Error getting column names from query.").run();
        }
        return names.toArray(new String[names.size()]);
    }
    
    /**
     * Builds a string array with the names of all tables in a schema.
     * @param db
     * @param schema
     * @return 
     */
    public static String[] getAllTableNames(DatabaseManager db, String schema)
    {
        String SQuery = "SELECT table_name FROM information_schema.tables "
                            + "where table_schema='" + schema + "'";
        
        return getter("TABLE_NAME", SQuery, db);
    }
    
    public static String[] getter(String column, String q, DatabaseManager db)
    {
        Wrapper wrap = null;
        ArrayList<String> names = new ArrayList<String>();
        
        try
        {
            wrap = db.getConnection();
            Statement stmt = ((Connection) wrap).createStatement();
            ResultSet rs = stmt.executeQuery(q);
            
            //Move to the first item.
            rs.beforeFirst();
            while (rs.next())
            {
                String s = rs.getString(column);
                if (s != null)
                {
                    s = s.toUpperCase();
                    if (CONSTANTS.accessLevelRequired(s, db.getAdminLevel()))
                    {
                        names.add(s);
                    }
                }
                else
                {
                    names.add(null);
                }
            }
            
        }
        catch (SQLException se)
        {
            //se.printStackTrace();
            new PopupMsg("No results.").run();
        }
        catch (Exception a)
        {
            //a.printStackTrace();
            new PopupMsg("Error.").run();
        }
        return names.toArray(new String[names.size()]);
    }
    
    /**
     * Counts the rows in a table.
     * @param rs
     * @return 
     */
    public static int getRowCount(ResultSet rs)
    {
        int row_num = 0;
        try
        {
            while (rs.next())
            {
                row_num++;
            }
        }
        catch(Exception e)
        {
            new PopupMsg("Error obtaining row count.").run();
        }
        return row_num;
    }
    
    public static Object[][] getCellData(ResultSet rs)
    {
        try{
            String[] columnNames = QueryTools.getColumnNames(rs, rs.getMetaData());
            int row_num = QueryTools.getRowCount(rs);
            int column_num = columnNames.length;
            return getCellData(rs, columnNames, row_num, column_num);
        }
        catch (SQLException se)
        {
            //se.printStackTrace();
            //System.err.println("No results.");
            new PopupMsg("No results.").run();
        }
        return null;
    }
    
    /**
     * Generates a multidimentional array of all data in a result set (essentially
     * the table results in array form).
     * @param rs
     * @param columnNames
     * @param row_num
     * @param column_num
     * @return 
     */
    public static Object[][] getCellData(ResultSet rs, String[] columnNames, int row_num, int column_num)
    {
        Object[][] cellData = new Object[row_num][column_num];
            
        try{
            //Move to the first item.
            rs.beforeFirst();
            
            for (int i = 0; i < cellData.length; i++)
            {
                if (rs.next())
                {
                    for (int j = 0; j < cellData[i].length; j++)
                    {
                        cellData[i][j] = rs.getString(columnNames[j]);
                    }
                }
            }
        }
        catch (SQLException se)
        {
            //se.printStackTrace();
            //System.err.println("No results.");
            new PopupMsg("No results.").run();
        }
        return cellData;
    }
    
    /**
     * Gets the primary key(s) from a table.
     * @param table
     * @return 
     */
    public static String[] getPrimaryKey(String table)
    {
        String q = "SHOW KEYS FROM " + table + " WHERE Key_name = 'PRIMARY'";
        DatabaseManager db = DatabaseManager.getInstance();
        String key = "";
        return getter("Column_Name", q, db);
    }
    
    /**
     * Gets the foreign key(s) from a table.
     * @param table
     * @return 
     */
    public static String[][] getForeignKeys(String table)
    {
        DatabaseManager db = DatabaseManager.getInstance();
        //This query gets a tuple that contains the table with a list of key attributes
        //and then the table referenced and the column name in the referenced table
        //If it's a primary key, the referenced table name will be null.
        //So if we want update a foreign key when we update something in a table,
        //We can run this query and then go over the rows, checking whether the
        //attribute is in the column name (hence a key) and then if the reference table
        //isn't null. If it isn't, then we save that as a table name, and get the
        //referenced column name. Then we insert the updated value into the referenced
        //table.
        
        String query = "SELECT COLUMN_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME " +
                        "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE " +
                        "WHERE TABLE_NAME = '" + table + "'";
        
        String[] colNames = getter("COLUMN_NAME", query, db);
        String[] refTableNames = getter("REFERENCED_TABLE_NAME", query, db);
        String[] refColNames = getter("REFERENCED_COLUMN_NAME", query, db);
        int len = colNames.length;
        
        String foreignKeys[][] = new String[len][len];
        
        for (int i = 0; i < len; i++)
        {
            for (int j = 0; j < len; j++)
            {
                foreignKeys[i] = new String[]{colNames[j], refTableNames[j], refColNames[j]};
            }
        }
                
        return foreignKeys;
    }
    
    /**
     * Determines whether a specific attribute is a foreign key.
     * @param foreignKeys
     * @param s
     * @return 
     */
    public static boolean isForeignKey(String[][] foreignKeys, String s)
    {
        int len = foreignKeys.length;
        for (int i = 0; i < len; i++)
        {
            for (int j = 0; j < len; j++)
            {
                if(foreignKeys[i][j].equalsIgnoreCase(s))
                {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Updates the value of an attribute that is the foreign key of a table.
     * This is dangerous depending on what the key is and there are some foreign
     * keys that shouldn't be allowed to change, but that's more complicated 
     * than what we did ...
     * @param foreignKeys
     * @param attribute
     * @param newValue
     * @param oldValue 
     */
    public static void updateForeignKey(String[][] foreignKeys, String attribute, 
            String newValue, String oldValue)
    {
        String foreignColumnName = "";
        String foreignTablename = "";
        String updateQuery = "";
        int len = foreignKeys.length;
        DatabaseManager db = DatabaseManager.getInstance();
        String schema = db.getSCHEMA();
        
        for (int i = 0; i < len; i++)
        {
            //Must be VERY careful not to allow an update if this isn't actually a foreign key.
            if(isForeignKey(foreignKeys, attribute) && foreignKeys[i][0].equalsIgnoreCase(attribute) 
                    && foreignKeys[i][1] != null)
            {
                foreignTablename = foreignKeys[i][1];
                foreignColumnName = foreignKeys[i][2];
                updateQuery = "Update " + schema + ".`" + foreignTablename + "` " +
                              "set `" + foreignColumnName + "` = '" + newValue + "' " +
                              "where `" + foreignColumnName + "` = '" + oldValue + "'";
                UpdateTable update = new UpdateTable(updateQuery);
            }
        }
    }
    
    /**
     * Filters text to prepare it for an SQL query -- Only use this when setString()
     * is not possible.
     * @param s
     * @return 
     */
    public static String sqlFilter(String s)
    {
        //Full list: https://dev.mysql.com/doc/refman/8.0/en/string-literals.html
        return s.replace("\\", "\\\\")
                .replace("'", "\\'")
                .replace("\"", "\\\"")
                .replace("%", "\\%")
                .replace("_", "\\_");
        
    }
    
}
