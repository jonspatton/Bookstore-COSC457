/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project.QueryForms;

import database.project.DatabaseManager;
import database.project.PopupMsg;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * @author Kyle Clabough
 * @author Jon S. Patton
 *
 */
public class ViewTable extends JFrame
{

    private JFrame inventoryFrame;
    private JTable inventoryTable;
    private DatabaseManager db;
    private int column_num;
    private String table;
    private int row_num;
    private String schema;
    private String SQuery;
    
    private String[] columnNames;
    private Object[][] cellData;

    /**
     * Gets everything from the specified table.
     * @param table 
     */
    public ViewTable(String table)
    {
        db = DatabaseManager.getInstance();
        schema = db.getSCHEMA();
        this.table = table;
        setSQuery("SELECT * FROM " + schema + ".`" + table + "`");
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    /**
     * Blank constructor.
     */
    public ViewTable(){
        db = DatabaseManager.getInstance();
        schema = db.getSCHEMA();
    }
    
    /**
     * Save a query.
     * @param query 
     */
    public void setSQuery(String query)
    {
        this.SQuery = query;
    }
    
    /**
     * Save which table this will display results from.
     * @param table 
     */
    public void setTable(String table)
    {
        this.table = table;
    }
    
    /**
     * @return the saved query.
     */
    public String getSQuery(){
        return this.SQuery;
    }
    
    /**
     * Show the results from a specified table.
     * @param table 
     */
    public void display(String table)
    {
        setTable(table);
        display();
    }
    
    /**
     * Build the results but don't display them yet.
     */
    public void gatherData()
    {
        Wrapper wrap = null;
        try
        {
            wrap = db.getConnection();
            Statement stmt = ((Connection) wrap).createStatement();
            ResultSet rs = stmt.executeQuery(SQuery);
            
            //String[] columnNames = QueryTools.getColumnNames(db, schema, table);
            columnNames = QueryTools.getColumnNames(rs, rs.getMetaData());
            row_num = QueryTools.getRowCount(rs);
            column_num = columnNames.length;
            cellData = QueryTools.getCellData(rs, columnNames, row_num, column_num);
        }
        catch (SQLException se)
        {
            //se.printStackTrace();
            //System.err.println("No results.");
            new PopupMsg("No results.").run();
        }
    }
    
    /**
     * @return a form table.
     */
    public JTable getTable()
    {
        
        
        Wrapper wrap = null;
        try
        {
            gatherData();
            
            inventoryTable = new JTable (cellData, columnNames);
            return inventoryTable;
        }
        catch (Exception a)
        {
            //a.printStackTrace();
            new PopupMsg("Error gathering data.").run();
        }
        return null;
    }
    
    /**
     * Show the results.
     */
    public void display()
    {
        inventoryFrame = new JFrame();
        inventoryFrame.setTitle("Inventory");
        inventoryFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        inventoryFrame.setSize(500, 500);
        inventoryFrame.setAlwaysOnTop(true);
        
        Wrapper wrap = null;
        try
        {
            gatherData();
            
            inventoryTable = new JTable (cellData, columnNames);
            inventoryFrame.add(new JScrollPane(inventoryTable));
            inventoryFrame.setVisible(true);
        }
        catch (Exception a)
        {
            //a.printStackTrace();
            new PopupMsg("Error gathering data.").run();
        }
    }
}
