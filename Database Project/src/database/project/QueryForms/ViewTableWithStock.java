/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project.QueryForms;

import database.project.DatabaseManager;

/**
 * *******UNUSED***** Meant to display a table with results from the stock table.
 * @author Jon Patton
 */
public class ViewTableWithStock extends ViewTable {
    
    String schema;
    
    /**
     * Gets results from the stock table and the corresponding product table.
     * @param table
     * @param itemNameType 
     */
    public ViewTableWithStock(String table, String itemNameType) {
        super();
        schema = DatabaseManager.getInstance().getSCHEMA();
        if (itemNameType.equalsIgnoreCase("title"))
        {
            super.setSQuery("SELECT `T.item_code`, `T.title`, `S.quantity`"
                + "FROM " + schema + ".`" + table + "` as T, stock as S "
                + "where (T.item_code = S.item_code)");
        }
        else if (itemNameType.equalsIgnoreCase("name"))
        {
            super.setSQuery("SELECT `T.item_code`, `T.name`, `S.quantity`"
                + "FROM " + schema + ".`" + table + "` as T, stock as S "
                + "where (T.item_code = S.item_code)");
        }        
    }
    
    
}
