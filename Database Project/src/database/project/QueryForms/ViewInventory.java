/*
 * COSC 457, Fall 2018 - Group I
 * Kyle Clabough, Jeremy Keith, Jon Patton, Hunter Wright
 */
package database.project.QueryForms;

import database.project.DatabaseManager;
import database.project.PopupMsg;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * @author Kyle Clabough
 * @author Jon S. Patton
 *
 */
public class ViewInventory extends JFrame
{

    private JFrame inventoryFrame;
    private JTable inventoryTable;
    private DatabaseManager db;
    static final int COLUMN_NUM = 5;
    private int row_num;
    private String schema;
    private String SQuery;

    /**
     * Default constructor: Gets everything from a table.
     */
    public ViewInventory(String table)
    {
        db = DatabaseManager.getInstance();
        schema = db.getSCHEMA();
        //ToDo: Set query that retrieves every type of product.
        //display();
    }
    
    /**
     * Blank constructor.
     */
    public ViewInventory(){
        db = DatabaseManager.getInstance();
        schema = db.getSCHEMA();
    }
    
    /**
     * Set the query for this.
     * @param query 
     */
    public void setSQuery(String query)
    {
        this.SQuery = query;
    }
    
    /**
     * @return the saved query.
     */
    public String getSQuery(){
        return this.SQuery;
    }
    
    /**
     * Show the results.
     */
    public void display()
    {
        inventoryFrame = new JFrame();
        inventoryFrame.setTitle("Inventory");
        inventoryFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        inventoryFrame.setSize(500, 500);
        inventoryFrame.setAlwaysOnTop(true);
        
        Wrapper wrap = null;
        try
        {
            wrap = db.getConnection();
            Statement stmt = ((Connection) wrap).createStatement();
            ResultSet rs = stmt.executeQuery(SQuery);
            ResultSetMetaData rsmd = rs.getMetaData();

            Statement find_rows = ((Connection) wrap).createStatement();
            ResultSet get_rows = find_rows.executeQuery(SQuery);
            row_num = 0;
            while (get_rows.next())
            {
                row_num++;
            }
            Object[][] cellData = new Object[row_num][COLUMN_NUM];
            String[] columnNames = new String[COLUMN_NUM];

            for (int i = 0; i < COLUMN_NUM; i++)
            {
                columnNames[i] = rsmd.getColumnName(i+1);
            }
            
            int i = 0;
            
            inventoryTable = new JTable (cellData, columnNames);
            inventoryFrame.add(new JScrollPane(inventoryTable));
            inventoryFrame.setVisible(true);
        }
        catch (SQLException se)
        {
            //se.printStackTrace();
            new PopupMsg("Error displaying or gathering data.").run();
        }
        catch (Exception a)
        {
            //a.printStackTrace();
            new PopupMsg("Error.").run();
        }

    }
}
